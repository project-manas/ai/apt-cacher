if [ $# -ne 1 ];
then
	echo "Usage: 'source setup <serverip>'"
else
	sudo /bin/su -c "echo 'Acquire::http { Proxy \"http://$1:9999\"; };' > /etc/apt/apt.conf.d/02proxy"
	sudo /bin/su -c "echo '' > /etc/apt/apt.conf.d/02proxy~"
	cat /etc/apt/apt.conf.d/02proxy

	sudo cp toggle-apt-cacher.sh /bin/toggle-apt-cacher.sh

	grep -q -F "alias toggle-apt-cache='/bin/toggle-apt-cacher.sh'" ~/.manas_aliases || echo -e "alias toggle-apt-cache='/bin/toggle-apt-cacher.sh'\n" >> ~/.manas_aliases

	grep -q -F 'if [ -f ~/.manas_aliases ]; then . ~/.manas_aliases; fi' ~/.bashrc || echo -e 'if [ -f ~/.manas_aliases ]; then . ~/.manas_aliases; fi\n' >> ~/.bashrc

	. ~/.bashrc

	echo 'toggle-apt-cacher has been installed'
	echo 'You can now use the toggle-apt-cacher from any terminal to toggle usage of the proxy'
fi
