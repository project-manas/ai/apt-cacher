About
===
Adds scripts and associated bash aliases to allow for convenient toggling usage of apt-cache server. 

Setup
===
Simply source the `setup.sh` with the cache server IP as an argument (`source setup.sh <your.server.ip>`) and your good to go!

Usage
===
From any terminal you can toggle whether apt should use the proxy server or not with the `toggle-apt-cacher` command.
This will also show you the current status of the proxy settings. A blank line would indicate that usage of the proxy has been disabled on your PC.
